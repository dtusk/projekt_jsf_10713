package strona.bazapracownikow.model.service;

import strona.bazapracownikow.model.domain.Worker;
import strona.bazapracownikow.utils.CrudRepository;

public interface WorkerRepository extends CrudRepository<Worker, Long> {
    public java.util.List<Worker> searchByLastName(String lastName);
    public java.util.List<Worker> searchByYearOfBirth(java.util.Date date);
}