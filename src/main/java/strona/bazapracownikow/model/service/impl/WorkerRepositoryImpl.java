/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strona.bazapracownikow.model.service.impl;

import strona.bazapracownikow.utils.HibernateSupport;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import strona.bazapracownikow.model.domain.Worker;
import strona.bazapracownikow.model.service.WorkerRepository;

@Service("workerRepository")
@Transactional
public class WorkerRepositoryImpl extends HibernateSupport implements WorkerRepository {

    @Override
    public List<Worker> searchByLastName(String lastName) {
        return getHibernate().createQuery("from Worker where lastName = :lastName")
                .setParameter("lastName", lastName)
                .list();
    }

    @Override
    public List<Worker> searchByYearOfBirth(Date date) {
        return getHibernate().createQuery("from Worker where yearOfBirth = :date")
                .setParameter("date", date)
                .list();
    }

    @Override
    public void create(Worker clazz) {
        getHibernate().save(clazz);
    }

    @Override
    public Worker read(Long id) {
        return (Worker) getHibernate().get(Worker.class, id);
    }

    @Override
    public List<Worker> readAll() {
        return getHibernate().createQuery("from Worker").list();
    }

    @Override
    public void update(Worker clazz) {
        getHibernate().update(clazz);
    }

    @Override
    public void delete(Worker clazz) {
        getHibernate().delete(clazz);
    }

}
