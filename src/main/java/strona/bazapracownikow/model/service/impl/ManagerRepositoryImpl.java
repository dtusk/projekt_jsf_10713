package strona.bazapracownikow.model.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import strona.bazapracownikow.model.domain.Manager;
import strona.bazapracownikow.model.service.ManagerRepository;
import strona.bazapracownikow.utils.HibernateSupport;

@Service("managerRepository")
@Transactional
public class ManagerRepositoryImpl extends HibernateSupport implements ManagerRepository {

    @Override
    public Manager validManager(String email, String password) {
        return (Manager) (!getHibernate()
                .createQuery("from Manager where email = :email and password = md5(:password)")
                .setParameter("email", email)
                .setParameter("password", password)
                .list().isEmpty() ? getHibernate()
                .createQuery("from Manager where email = :email and password = md5(:password)")
                .setParameter("email", email)
                .setParameter("password", password)
                .list().get(0) : null) ; // :) inaczej wywala nullpointerexception jezeli nie znajduje
    }
    
    @Override
    public boolean isManagerDuplicated(String email) {
        return !getHibernate().createQuery("from Manager where email = :email")
                .setParameter("email", email)
                .list().isEmpty();
    }

    @Override
    public void create(Manager clazz) {
        getHibernate().save(clazz);
    }

    @Override
    public Manager read(Long id) {
        return (Manager) getHibernate().get(Manager.class, id);
    }

    @Override
    public List<Manager> readAll() {
        return getHibernate().createQuery("from Manager").list();
    }

    @Override
    public void update(Manager clazz) {
        getHibernate().update(clazz);
    }

    @Override
    public void delete(Manager clazz) {
        getHibernate().delete(clazz);
    }
    
}
