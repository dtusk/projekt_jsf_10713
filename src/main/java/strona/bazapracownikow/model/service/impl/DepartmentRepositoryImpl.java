/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package strona.bazapracownikow.model.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import strona.bazapracownikow.model.domain.Department;
import strona.bazapracownikow.model.service.DepartmentRepository;
import strona.bazapracownikow.utils.HibernateSupport;

@Service("departmentRepository")
@Transactional
public class DepartmentRepositoryImpl extends HibernateSupport implements DepartmentRepository {

    @Override
    public boolean isDepartmentDuplicated(String name) {
        return !getHibernate().createQuery("from Department where name = :name")
                .setParameter("name", name).list().isEmpty();
    }

    @Override
    public void create(Department clazz) {
        getHibernate().save(clazz);
    }

    @Override
    public Department read(Long id) {
        return (Department) getHibernate().get(Department.class, id);
    }

    @Override
    public List<Department> readAll() {
        return getHibernate().createQuery("from Department").list();
    }

    @Override
    public void update(Department clazz) {
        getHibernate().update(clazz);
    }

    @Override
    public void delete(Department clazz) {
        getHibernate().delete(clazz);
    }

}
