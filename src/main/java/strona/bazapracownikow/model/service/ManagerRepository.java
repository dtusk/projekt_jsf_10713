package strona.bazapracownikow.model.service;

import strona.bazapracownikow.model.domain.Manager;
import strona.bazapracownikow.utils.CrudRepository;

public interface ManagerRepository extends CrudRepository<Manager, Long>{
    public Manager validManager(String email, String password);
    public boolean isManagerDuplicated(String email);
}
