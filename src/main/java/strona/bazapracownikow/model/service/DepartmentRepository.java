/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package strona.bazapracownikow.model.service;

import strona.bazapracownikow.model.domain.Department;
import strona.bazapracownikow.utils.CrudRepository;

/**
 *
 * @author shandnyar
 */
public interface DepartmentRepository extends CrudRepository<Department, Long>{
    public boolean isDepartmentDuplicated(String name);
}
