package strona.bazapracownikow.model.domain;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Manager extends EntityImpl{
    
    @Column(nullable = false, unique = true, updatable = false)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    
    private boolean stillWorking;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isStillWorking() {
        return stillWorking;
    }

    public void setStillWorking(boolean stillWorking) {
        this.stillWorking = stillWorking;
    }
    
    
    
}
