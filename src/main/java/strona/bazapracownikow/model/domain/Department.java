package strona.bazapracownikow.model.domain;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

/**
 *
 * @author shandnyar
 */
@Entity
public class Department extends EntityImpl { 
    
    @Column(nullable = false, unique = true)
    private String name;
    
    @OneToMany(mappedBy = "department", fetch = FetchType.EAGER)
    private Set<Worker> workers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Worker> getWorkers() {
        return workers;
    }

    public void setWorkers(Set<Worker> workers) {
        this.workers = workers;
    }
    
    
}
