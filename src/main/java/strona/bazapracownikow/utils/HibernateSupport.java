/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package strona.bazapracownikow.utils;

import javax.persistence.MappedSuperclass;
import javax.persistence.PostLoad;
import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author shandnyar
 */
@MappedSuperclass
public abstract class HibernateSupport {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    public Session getHibernate() {
        return sessionFactory.isClosed() ? sessionFactory.openSession() : sessionFactory.getCurrentSession();
    }
    
    @PostLoad
    @PostPersist
    @PostRemove
    @PostUpdate
    public void onPostEverything() {
        if (!sessionFactory.isClosed())
            sessionFactory.close();
    }
}
