package strona.bazapracownikow.utils;

public interface CrudRepository<Class, Serializable> {
    public void create(Class clazz);
    public Class read(Serializable id);
    public java.util.List<Class> readAll();
    public void update(Class clazz);
    public void delete(Class clazz);
}