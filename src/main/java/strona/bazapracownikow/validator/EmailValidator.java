package strona.bazapracownikow.validator;

import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class EmailValidator implements Validator {

    public void validate(FacesContext ctx, UIComponent uic, Object o)
            throws ValidatorException {
        if (!((String) o).contains("@")) {
            ResourceBundle bundle = ctx.getApplication().getResourceBundle(ctx, "msg");
            String message = bundle.getString("notValidEmailContained");
            FacesMessage fm = new FacesMessage(message);
            ctx.addMessage(uic.getClientId(), fm);
            throw new ValidatorException(fm);
        }
    }
}
