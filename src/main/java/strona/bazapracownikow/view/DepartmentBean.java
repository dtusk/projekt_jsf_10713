package strona.bazapracownikow.view;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import strona.bazapracownikow.model.domain.Department;
import strona.bazapracownikow.model.service.DepartmentRepository;

/**
 *
 * @author shandnyar
 */
@Component
@Named
@RequestScoped
public class DepartmentBean implements Serializable {

    /**
     * Creates a new instance of DepartmentBean
     */
    @Autowired
    private DepartmentRepository departmentRepository;

    @NotNull
    private String name;

    private Department department;
    
    private UIComponent component;
    
    public DepartmentBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public Department getDepartment() {
        return department;
    }

    public String addDepartment() {
        if (departmentRepository.isDepartmentDuplicated(name)) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            String message = ctx.getApplication().getResourceBundle(ctx, "msg").getString("departmentDuplicated");
            ctx.addMessage(component.getClientId(), new FacesMessage(message));
            return "";
        }
        Department departmentCreate = new Department();
        departmentCreate.setName(name);
        departmentRepository.create(departmentCreate);
        clear();
        return "showDepartments.jsf?faces-redirect=true";
    }
    
    public String showDepartment(long id) {
        department = departmentRepository.read(id);
        return "showDepartment.jsf";
    }
    
    public List<Department> getDepartments() {
        return departmentRepository.readAll();
    }
    
    public Map<String, Serializable> asMap() {
        Map results = new LinkedHashMap();
        for (Department d : departmentRepository.readAll()) {
            results.put(d.getName(), d.getId());
        }
        return results;
    }
    
    private void clear() {
        name = null;
    }
}
