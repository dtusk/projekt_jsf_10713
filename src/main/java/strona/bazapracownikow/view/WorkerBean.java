package strona.bazapracownikow.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import strona.bazapracownikow.model.domain.Worker;
import strona.bazapracownikow.model.service.DepartmentRepository;
import strona.bazapracownikow.model.service.WorkerRepository;

/**
 *
 * @author shandnyar
 */
@Component
@Named
@RequestScoped
public class WorkerBean implements Serializable {

    /**
     * Creates a new instance of WorkerBean
     */
    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    private String firstName;
    private String lastName;
    private Date dateOfBirth;
    private double salary;
    private long departmentId;

    public WorkerBean() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public List<Worker> getWorkers() {
        return workerRepository.readAll();
    }

    public long getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(long departmentId) {
        this.departmentId = departmentId;
    }

    public String addWorker() {
        Worker worker = new Worker();
        worker.setFirstName(firstName);
        worker.setLastName(lastName);
        worker.setSalary(salary);
        worker.setDateOfBirth(dateOfBirth);
        worker.setDepartment(departmentRepository.read(departmentId));
        worker.setStillWorking(false);
        workerRepository.create(worker);
        clear();
        return "showWorkers.jsf?faces-redirect=true";
    }
    
    public String delWorker(Worker worker) {
        workerRepository.delete(worker);
        return null;
    }

    public void departmentChanged(ValueChangeEvent e) {
        departmentId = (Long) e.getNewValue();
    }

    private void clear() {
        firstName = lastName = null;
        departmentId = 0l;
        salary = 0;
        dateOfBirth = null;
    }

}
