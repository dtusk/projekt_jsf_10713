package strona.bazapracownikow.view;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import strona.bazapracownikow.model.domain.Manager;
import strona.bazapracownikow.model.service.ManagerRepository;
import strona.bazapracownikow.utils.HashUtil;

/**
 *
 * @author shandnyar
 */
@Component
@Named
@RequestScoped
public class ManagerBean implements Serializable {
    
    @Autowired
    private ManagerRepository managerRepository;

    private String email;
    @Size(min = 6, max = 20)
    private String password;
    private String firstName;
    private String lastName;
    
    private UIComponent component;
    
    public ManagerBean() {
    }

    public String addManager() {
        if (managerRepository.isManagerDuplicated(email)) {
            FacesContext ctx = FacesContext.getCurrentInstance();
            ResourceBundle bundle = ctx.getApplication().getResourceBundle(ctx, "msg");
            String message = bundle.getString("emailDuplicated");
            ctx.addMessage(component.getClientId(), new FacesMessage(message));
            return "";
        }
        Manager manager = new Manager();
        manager.setEmail(email);
        manager.setPassword(HashUtil.crypt(password));
        manager.setFirstName(firstName);
        manager.setLastName(lastName);
        manager.setStillWorking(true);
        managerRepository.create(manager);
        clear();
        return "registerSuccess.jsf";
    }

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    private void clear() {
        email = firstName = lastName = password = null;
    }
}
