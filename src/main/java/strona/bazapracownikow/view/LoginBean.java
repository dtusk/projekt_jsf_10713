package strona.bazapracownikow.view;

import java.io.Serializable;
import java.util.ResourceBundle;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.validation.constraints.Size;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import strona.bazapracownikow.model.domain.Manager;
import strona.bazapracownikow.model.service.ManagerRepository;

@Component
@Named
@SessionScoped
public class LoginBean implements Serializable {

    @Autowired
    private ManagerRepository managerRepository;

    private String email;
    @Size(min=3)
    private String password;

    private Manager manager;

    private UIComponent component;

    public UIComponent getComponent() {
        return component;
    }

    public void setComponent(UIComponent component) {
        this.component = component;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /*
    
    pare złów. według mnie, korzystanie z listenera jest czymś wolnym.
    
    */
    public String valid() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (null != (manager = managerRepository.validManager(email, password))) {
            ctx.getApplication().getNavigationHandler().handleNavigation(ctx, null, "index.jsf");
            return "index.jsf?faces-redirect=true";
        } else {
            ResourceBundle bundle = ctx.getApplication().getResourceBundle(ctx, "msg");
            String message = bundle.getString("failedLogin");
            ctx.addMessage(component.getClientId(), new FacesMessage(message));
            return null;
        }
    }
    
    public String logout() {
        manager = null;
        return "index.jsf?faces-redirect=true";
    }

    public Manager getManager() {
        return manager;
    }

}
