package strona.bazapracownikow.view;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

/**
 *
 * @author shandnyar
 */
@Named
@SessionScoped
public class LanguageBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String localeCode;

    private static Map<String, Locale> countries;

    static {
        countries = new LinkedHashMap<String, Locale>();
        countries.put("Polski", new Locale("pl"));
//        countries.put("English", Locale.ENGLISH);
        countries.put("Deutsch", Locale.GERMAN);
    }

    public Map<String, Locale> getCountriesInMap() {
        return countries;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    //value change event listener
    public void countryLocaleCodeChanged(ValueChangeEvent e) {

        localeCode = e.getNewValue().toString();

        //loop country map to compare the locale code
        for (Map.Entry<String, Locale> entry : countries.entrySet()) {

            if (entry.getValue().toString().equals(localeCode)) {

                FacesContext.getCurrentInstance()
                        .getViewRoot().setLocale((Locale) entry.getValue());

            }
        }
    }

}
